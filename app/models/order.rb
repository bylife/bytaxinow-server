class Order
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Geospatial
  belongs_to :customer, index: true
  belongs_to :area, index: true
  belongs_to :driver, index: true
  
  field :customer
  field :area
  field :driver
  field :order_id, type: Integer
  field :location, type: Point
  field :observation, type: String
  field :status, type: String
end

require 'carrierwave/mongoid'
class Customer
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Geospatial
  
  field :name
  field :email
  field :phone
  field :address
  field :city
  field :state
  field :location, type: Point

  mount_uploader :avatar, AvatarUploader
  validates_presence_of :name, :phone
end
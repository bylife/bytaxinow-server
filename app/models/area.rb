class Area
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Geospatial
  field :title
  field :description
  field :center, type: Hash, spatial: true
  field :paths,	type: Hash, spatial: true  
end

class Driver
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  belongs_to :unity, index: true
  
  field :name
  field :avatar
  field :phone
  field :birth, type: Date
  field :document
  field :email

  embedded_in :unity, :inverse_of => :drivers
end

require 'carrierwave/mongoid'
class Unity
  include Mongoid::Document
  
  field :prefix, type: Integer
  field :owner_name
  field :owner_email
  field :owner_phone
  field :document
  field :car_model
  field :license
  field :color
  field :active, type: Mongoid::Boolean, default: false
  field :qrx, type: Mongoid::Boolean, default: false


  mount_uploader :avatar, AvatarUploader
  validates_presence_of :prefix, :owner_name, :document, :car_model, :license
  embeds_many :drivers
end

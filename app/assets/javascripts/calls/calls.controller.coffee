angular.module 'bytaxiServer'
	.controller 'CallsController', ['$scope', '$modal', 'Customer' , ($scope, $modal, Customer) ->
		$scope.today = new Date()
		d = new Date()
		$scope.yesterday = d.setDate(d.getDate() - 1)

		$scope.getCustomer = (val) ->
			Customer.query({phone: val}).then (data) ->
				data.map (customer) ->
					customer.name
					console.log customer

		$scope.newOrder = () ->
			modalInstance = $modal.open
				animation: true,
				templateUrl: 'assets/templates/new-order.html',
				controller: 'newOrderController',

			modalInstance.result.then (data) ->
				$scope.orders.push data

		$scope.orders = [
			{
				datetime : '2015-08-09T05:05:33.052Z',
				requester: {name: 'Nadia Ali', phone: '+5512988046754', avatar: 'assets/users/user3.jpg'},
				driver: {name: 'João da Silva', number: '33', avatar: 'assets/users/user5.jpg'},
				origin: {address: 'Rua Francisco Alves Pereira, 289 - Centro', lat: -23.87663, lng: -45.077655},
				destiny: {address: 'Rua dos Manacas, 1002 - Jaboticabeiras', lat: -23.07655, lng: -45.23332}
			},
			{
				datetime : '2015-08-08T06:05:33.052Z',
				requester: {name: 'Maria Clara Souza', phone: '+5512988046754', avatar: 'assets/users/user2.jpg'},
				driver: {name: 'João da Silva', number: '33', avatar: 'assets/users/user5.jpg'},
				origin: {address: 'Rua Doutor Pedro Costa, 889 - Centro', lat: -23.87663, lng: -45.077655},
				destiny: {address: 'Rua Ary Barroso Filho, 782 - Parque Três Marias', lat: -23.07655, lng: -45.23332}
			}
		]
	]
	.controller 'newOrderController', ['$scope', '$modalInstance', 'Customer' , ($scope, $modalInstance, Customer) ->

	]
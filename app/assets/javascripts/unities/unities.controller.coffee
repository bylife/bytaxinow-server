angular.module "bytaxiServer"
	.controller "UnitiesController", ['$scope', 'Unity', ($scope, Unity) ->
		Unity.query().then (unities) ->
			$scope.unities = unities
	]
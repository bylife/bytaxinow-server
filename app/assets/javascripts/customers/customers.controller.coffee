angular.module 'bytaxiServer'
	.controller 'CustomersController', ['$scope', 'Customer', '$modal', ($scope, Customer, $modal) ->
		$scope.customers = []
		Customer.query().then (customers) ->
			$scope.customers = customers

		$scope.newCustomer = () ->
			modalInstance = $modal.open
				animation: true,
				templateUrl: 'assets/templates/new-customer.html',
				controller: 'newCustomerController',
				size: 'lg'
			modalInstance.result.then (data) ->
				$scope.customers.push data
	]

	.controller 'newCustomerController', ['$scope', 'Customer', '$modalInstance', '$http', ($scope, Customer, $modalInstance, $http) ->
		$scope.states = [{id: 'AC', title: 'AC'},{id: 'AL', title: 'AL'},{id: 'AM', title: 'AM'},{id: 'AP', title: 'AP'},{id: 'BA', title: 'BA'},{id: 'CE', title: 'CE'},{id: 'DF', title: 'DF'},{id: 'ES', title: 'ES'},{id: 'GO', title: 'GO'},{id: 'MA', title: 'MA'},{id: 'MG', title: 'MG'},{id: 'MS', title: 'MS'},{id: 'MT', title: 'MT'},{id: 'PA', title: 'PA'},{id: 'PB', title: 'PB'},{id: 'PE', title: 'PE'},{id: 'PI', title: 'PI'},{id: 'PR', title: 'PR'},{id: 'RJ', title: 'RJ'},{id: 'RN', title: 'RN'},{id: 'RO', title: 'RO'},{id: 'RR', title: 'RR'},{id: 'RS', title: 'RS'},{id: 'SC', title: 'SC'},{id: 'SE', title: 'SE'},{id: 'SP', title: 'SP'},{id: 'TO', title: 'TO'}]
		$scope.customer = {
			city: 'Taubaté',
			state: 'SP',
			location: { 'lat': -23.015959, 'lng': -45.5192825}
		}


		$scope.getLocation = (val) ->						
			return $http.get 'https://maps.googleapis.com/maps/api/geocode/json', 
				params: {
					address: val,
					key: 'AIzaSyBmtW0fQdXEvtWNzxrkYlePjHf2-ffce64'
				},
				headers : {								
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin' : '*',
				}
			.then (response) ->
				return response.data.results.map (item) ->
					console.log item
					return item.formatted_address

		$scope.saveCustomer = () ->
			#$scope.customer.location = get_latlng()
			customer = new Customer($scope.customer).create()
			$modalInstance.close(customer)

		$scope.cancel = () ->
			$modalInstance.dismiss('cancel')
	]
		
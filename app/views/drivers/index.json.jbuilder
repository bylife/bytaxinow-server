json.array!(@drivers) do |driver|
  json.extract! driver, :id, :name, :birth, :document, :email
  json.url driver_url(driver, format: :json)
end

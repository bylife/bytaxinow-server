json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :email, :avatar, :phone, :address, :city, :state, :location
  json.url customer_url(customer, format: :json)
end

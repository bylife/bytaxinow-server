json.array!(@orders) do |order|
  json.extract! order, :id, :order_id, :customer, :area, :driver, :location, :observation, :status
  json.url order_url(order, format: :json)
end

json.array!(@unities) do |unity|
  json.extract! unity, :id, :prefix, :owner_name, :owner_email, :owner_phone, :car_model, :license, :color, :avatar, :active, :qrx
  json.url unity_url(unity, format: :json)
end

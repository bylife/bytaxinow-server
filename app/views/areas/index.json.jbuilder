json.array!(@areas) do |area|
  json.extract! area, :id, :title, :description, :paths, :center
  json.url area_url(area, format: :json)
end

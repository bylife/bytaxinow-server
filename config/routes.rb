Rails.application.routes.draw do
  
  resources :orders
  resources :areas

  resources :unities do
    resources :drivers
  end

  resources :customers
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  resources :home, only: [:index]
  root to: 'home#index'  
  #resources :users
  
end
